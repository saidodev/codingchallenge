package com.echallegecode.ur.dao;

import com.echallegecode.ur.entities.User;

public interface IUserDao extends IGenericDao<User> {

}
