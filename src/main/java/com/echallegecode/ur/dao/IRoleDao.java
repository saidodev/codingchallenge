package com.echallegecode.ur.dao;

import com.echallegecode.ur.entities.Role;

public interface IRoleDao extends IGenericDao<Role>{

}
