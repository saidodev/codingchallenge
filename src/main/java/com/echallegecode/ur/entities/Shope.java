package com.echallegecode.ur.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Shope implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private int id_shope;
	private String name;
	private String image;
	private String description;
	private Boolean liked;
	
	
	public Shope() {
		// TODO Auto-generated constructor stub
	}
	public Shope(int id_shope, String name, String image) {
		super();
		this.id_shope = id_shope;
		this.name = name;
		this.image = image;
	}
	public int getId_shope() {
		return id_shope;
	}
	public void setId_shope(int id_shope) {
		this.id_shope = id_shope;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getLiked() {
		return liked;
	}
	public void setLiked(Boolean liked) {
		this.liked = liked;
	}
	
	
	
	

}
