package com.echallegecode.ur.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Role implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id_Role;
	
	private String name;
	

	
	
	public Role() {
		// TODO Auto-generated constructor stub
	}

	public Long getId_Role() {
		return id_Role;
	}

	public void setId_Role(Long id_Role) {
		this.id_Role = id_Role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	
	
	
	

}
