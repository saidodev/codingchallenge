package com.echallegecode.ur.entities;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class User  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id_user;
	
	private String mail;
	
	private String password;
	
	private Boolean actived;
	
	
	

	public Boolean getActived() {
		return actived;
	}


	public void setActived(Boolean actived) {
		this.actived = actived;
	}


	public User() {
		// TODO Auto-generated constructor stub
	}
	

	public User(Long id_user, String mail, String password) {
		super();
		this.id_user = id_user;
		this.mail = mail;
		this.password = password;
	}


	public Long getId_user() {
		return id_user;
	}

	public void setId_user(Long id_user) {
		this.id_user = id_user;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

	
	
	

}
