package com.echallegecode.ur.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.echallegecode.ur.entities.User;
import com.echallegecode.ur.service.IRoleService;
import com.echallegecode.ur.service.IUserService;

@Controller
@RequestMapping("/signup")
public class UserController {
	
	@Autowired	
	private IUserService userService;
	
	
	@RequestMapping(value="/",method = RequestMethod.GET)
	public String signup(Model model)
	{
		User user=new User();
		
		model.addAttribute("user", user);
		return "signup/signup";
	}
	
	@RequestMapping(value = "/enregistrer",method=RequestMethod.POST)
	  public String enregistrerConducteur(Model model, User user) {
	
		user.setActived(true);
		//user.setMail(mail);
		System.out.println("mail");
		
		
		if(user != null)	{
		 
		
			userService.save(user);
		}
		
		
		return "login/login";
	}

}
