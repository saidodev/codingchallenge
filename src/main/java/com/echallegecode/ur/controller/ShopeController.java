package com.echallegecode.ur.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.echallegecode.ur.entities.Shope;
import com.echallegecode.ur.service.IShopeService;


@Controller
@RequestMapping(value="/shope")
public class ShopeController {
	@Autowired	
	private IShopeService iShopeService;
	
	//get department//
	
			@RequestMapping(value = "/")
			public String shopeNearby(Model model) {
				List<Shope> shopes = iShopeService.selectAll();
				if (shopes == null) {
					shopes = new ArrayList<Shope>();
				}
				model.addAttribute("shopes", shopes);
				
				return "shope/nearby";
			}
			
			
			
	

}
