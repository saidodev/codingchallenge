package com.echallegecode.ur.service;

import java.util.List;

import com.echallegecode.ur.entities.Role;

public interface IRoleService {
	
	public Role save(Role entity);
	
	public Role update(Role entity);

	public List<Role> selectAll();

	public List<Role> selectAll(String sortField, String sort);

	public Role getById(Long id);

	public void remove(Long id);

	public Role findOne(String paramName, Object paramValue);

	public Role findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);



}
