package com.echallegecode.ur.service;

import java.util.List;

import com.echallegecode.ur.entities.User;


public interface IUserService {
	
	public User save(User entity);
	
	public User update(User entity);

	public List<User> selectAll();

	public List<User> selectAll(String sortField, String sort);

	public User getById(Long id);

	public void remove(Long id);

	public User findOne(String paramName, Object paramValue);

	public User findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);


}
