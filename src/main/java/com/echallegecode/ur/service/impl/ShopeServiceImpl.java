package com.echallegecode.ur.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.echallegecode.ur.dao.IShopeDao;
import com.echallegecode.ur.entities.Shope;
import com.echallegecode.ur.service.IShopeService;

@Transactional
public class ShopeServiceImpl implements IShopeService{
	
	private IShopeDao dao;
	
	public void setDao(IShopeDao dao) {
		this.dao = dao;
	}

	@Override
	public Shope save(Shope entity) {
		return dao.save(entity);
	}

	@Override
	public Shope update(Shope entity) {
		return dao.update(entity);
	}

	@Override
	public List<Shope> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Shope> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Shope getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Shope findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Shope findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


}
