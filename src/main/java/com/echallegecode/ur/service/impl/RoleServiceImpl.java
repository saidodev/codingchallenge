package com.echallegecode.ur.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import com.echallegecode.ur.dao.IRoleDao;
import com.echallegecode.ur.entities.Role;
import com.echallegecode.ur.service.IRoleService;

@Transactional
public class RoleServiceImpl implements IRoleService{
	
	private IRoleDao dao;
	
	public void setDao(IRoleDao dao) {
		this.dao = dao;
	}

	@Override
	public Role save(Role entity) {
		return dao.save(entity);
	}

	@Override
	public Role update(Role entity) {
		return dao.update(entity);
	}

	@Override
	public List<Role> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Role> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Role getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Role findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Role findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
