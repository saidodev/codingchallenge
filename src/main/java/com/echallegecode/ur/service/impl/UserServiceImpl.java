package com.echallegecode.ur.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.echallegecode.ur.dao.IUserDao;
import com.echallegecode.ur.entities.User;
import com.echallegecode.ur.service.IUserService;

@Transactional
public class UserServiceImpl implements IUserService{
	
	private IUserDao dao;
	
	public void setDao(IUserDao dao) {
		this.dao = dao;
	}

	@Override
	public User save(User entity) {
		return dao.save(entity);
	}

	@Override
	public User update(User entity) {
		return dao.update(entity);
	}

	@Override
	public List<User> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<User> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public User getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public User findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public User findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


}
