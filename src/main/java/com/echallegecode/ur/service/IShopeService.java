package com.echallegecode.ur.service;

import java.util.List;

import com.echallegecode.ur.entities.Shope;

public interface IShopeService {
	
	public Shope save(Shope entity);
	
	public Shope update(Shope entity);

	public List<Shope> selectAll();

	public List<Shope> selectAll(String sortField, String sort);

	public Shope getById(Long id);

	public void remove(Long id);

	public Shope findOne(String paramName, Object paramValue);

	public Shope findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, String paramValue);



}
