<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nearby</title>

    <!-- Bootstrap Core CSS -->
   
     <!-- Bootstrap core CSS -->
    <link href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<%=request.getContextPath() %>/resources/css/heroic-features.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <style>
.fa {
  font-size: 50px;
  cursor: pointer;
  user-select: none;
}

.fa:hover {
  color: darkblue;
}
</style>
    

   
</head>

<body>


        <!-- Navigation -->
            
            <%@ include file="/WEB-INF/views/menu_top/topMenu.jsp" %>
			
        <!-- Navigation -->  
       
       
         <!-- Content -->
            
            
             <!-- Page Content -->
    <div class="container">

      <!-- Jumbotron Header -->
      <br> <br> <br> 

      <!-- Page Features -->
      <div class="row text-center">

        <c:forEach items="${shopes}" var="shope">
        
	        <div class="col-lg-3 col-md-6 mb-4">
	          <div class="card">
	            <img class="card-img-top" src="<%=request.getContextPath() %>/resources/images/${shope.getImage()}" >
	            <div class="card-body">
	              <h4 class="card-title">${shope.getName()}</h4>
	              <p class="card-text">${shope.getDescription()}</p>
	            </div>
	            <div class="card-footer">
	              <div class="btn-group" role="group">
	              		
	              		<c:choose>
	              			<c:when test="${shope.getLiked()==true}">
	              				<button onclick="myFunction(this)" class="fa fa-thumbs-up" id="${shope.getId_shope()}"></button>
	              			</c:when>
	              			
	              			<c:otherwise>
	              				<button onclick="myFunction(this)" class="fa fa-thumbs-down" id="${shope.getId_shope()}"></button>
	              			</c:otherwise>
	              		</c:choose>
	              		
	              	
					</div>
	            </div>
	          </div>
	          
	        </div>
        </c:forEach>

    </div>
            
  </div>
			
      
	    <script>
			function myFunction(x) {
				x.classList.toggle("fa-thumbs-down");
	  			x.classList.toggle("fa-thumbs-up");
									}
		</script>
		
	
            
            <%@ include file="/WEB-INF/views/footer/footer.jsp" %>
			
        <!-- Footer --> 
    <!-- Bootstrap core JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   

</body>

</html>
