<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<!------ Include the above in your HEAD tag ---------->
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<%=request.getContextPath() %>/resources/images/icons/favicon.ico"/>


	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/css/util.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/css/main.css">
	  <%@ include file="/WEB-INF/views/includes/includes.jsp" %>
	
<!--===============================================================================================-->

	
</head>
<body>


				<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
			<div class="login100-pic js-tilt" data-tilt>
				</div>
				
				
	            <c:url value="/signup/enregistrer" var ="urlEnregistrer" />
				<f:form  modelAttribute="user" action="${urlEnregistrer}"> 
					<f:hidden path="id_user"/>
					<span class="login100-form-title">
						Sign Up
					</span>

					<div class="wrap-input100 validate-input" >
						<f:input path="mail" name="mail" class="input100" type="email"  placeholder="Email"/>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" >
						<f:input path="password" class="input100" type="password"  placeholder="Password"/>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<f:hidden path="actived"/>
					
				
					
					
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn">
							Sign Up
						</button>
					</div>

					
			
				</f:form>
			</div>
		</div>
	</div>
	
	
	<!-- Modal Registration -->
	

	
	<!-- Modal Registration -->
	
	
	<!--===============================================================================================-->	
	<script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/popper.js"></script>
	<script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<%=request.getContextPath() %>/resources/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<%=request.getContextPath() %>/resources/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>			
    
    <!-- Bootstrap core JavaScript -->
    <script src="<%=request.getContextPath() %>/resources/vendor/jquery/jquery.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
</body>
</html>